name = "Janie"
age = 27
occupation = "IT Instructor"
movie = "White Chicks"
rating = 90.0

print(f"I am {name}, and I am {age}, I work as a {occupation}, and my rating for {movie} is {rating}%")

num1 = 7
num2 = 5
num3 = 2

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)